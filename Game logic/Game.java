import java.awt.event.*;
import java.util.*;


public class Game { 
	
	private String gameType;
	public ArrayList<GameCharacter> playerTeam;
	public ArrayList<GameCharacter> AITeam;
	public BattleGrid grid;

	
	public Game (String s) { //Constructor for the Game object
	gameType = s; //Used for calls to certain classes	

	playerTeam = FillPlayerTeam(gameType); //Fills Player team ArrayList
	AITeam = FillAITeam(gameType); //Fills AI team ArrayList
	grid = new BattleGrid(gameType); //Creates Grid object
	}

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	                                         Methods called by constructor                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

private ArrayList<GameCharacter> FillPlayerTeam(String s) { //Fills the player team ArrayList 
	ArrayList<GameCharacter> list = new ArrayList<GameCharacter>();
	if (s.contentEquals("Demo") || s.equals("Forest_Fight")) { //allows for different configurations based on the selected game type
		list.add(new Fighter());
		list.add(new Rogue());
		list.add(new Mage());
		list.add(new Cleric());
		
	}
	
	try {
		Scanner XPosReader = new Scanner (new java.io.FileReader("src/" + s + "/" + s + "XVal.txt"));
	
		for (GameCharacter c : list) { //allows for dynamic setting of stating locations based on .txt int values 
			int x = XPosReader.nextInt();
			c.setX(x);
			c.setY(20);
		}
		
    
	XPosReader.close();
	}
	
	catch (java.io.FileNotFoundException e) {
		System.out.println ("Error opening file for player team " + s);
		System.exit(1);
	}
	
	return list;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

private ArrayList<GameCharacter> FillAITeam(String s) {
	ArrayList<GameCharacter> list = new ArrayList<GameCharacter>();
	if (s.contentEquals("Demo") || s.equals("Forest_Fight")) { //allows for different configurations based on the selected game type
		list.add(new Fighter());
		list.add(new Rogue());
		list.add(new Mage());
		list.add(new Cleric());
		
	}
	
	try {
		Scanner XPosReader = new Scanner (new java.io.FileReader("src/" + s + "/" + s + "XVal.txt"));

	
		for (GameCharacter c : list) { //allows for dynamic setting of stating locations based on .txt int values
			int x = XPosReader.nextInt();
			c.setX(x);
			c.setY(3);
			c.isAIChar();
		}
		
    
	XPosReader.close();
	}
	
	catch (java.io.FileNotFoundException e) {
		System.out.println ("Error opening file AI team " + s);
		System.exit(1);
	}
	
	return list;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                               Player GAME LOGIC                                                          //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void Movement (GameCharacter c, MouseEvent e) { //Movement method for the player owned game characters 
	
	if (c.getHasMoved() == false && c.getIsAlive() == true) { //Checks to ensure the selected characters are alive and hve not moved
		int currentXPos = c.getX();
		int currentYPos = c.getY();
		int newXPos = e.getX()/32;
		int newYPos = e.getY()/32;

 		
		if(newXPos <= 23 && newYPos <= 23) { //converts the mouse point passed into the method to a 0-23 int value reflecting the board size
			int collis = Main.g.grid.Layer4.a[newXPos][newYPos]; //gets the collision value in layer 4 (where 0 is a square able to be occupied and 1 is not) 

			
			if(collis == 0){ //Performs the check on the collis value
				boolean inrange = checkmoverange(c,e);
				if(inrange){ //ensures the target square is in range of the selected game character
					c.setX(newXPos);
					c.setY(newYPos);
					c.setHasMoved(true);
					Main.g.grid.Layer4.a[newXPos][newYPos] = 1; //sets the new square as occupied to prevent other game characters from stacking in same square
					Main.g.grid.Layer4.a[currentXPos][currentYPos] = 0; //frees the previous occupied square for others to move to
					Main.battleSpace.repaint(); //updates the visuals
					
				}
				else
					Main.battleSpace.gameOutputs.append("That location is out of move range; ");
			
			}
			else
				Main.battleSpace.gameOutputs.append("Tile is not valid move location; ");
			
		}
		else
			Main.battleSpace.gameOutputs.append("Location is not on the battlefield; ");
	}
	
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public boolean checkmoverange(GameCharacter c, MouseEvent e) { //checks the movement range in relation to the specified game character and the target square
	
	for (int i = c.getX()-c.getMoveDistance(); i <= c.getX()+c.getMoveDistance(); ++i) {
		for (int j = c.getY()-c.getMoveDistance(); j <= c.getY()+c.getMoveDistance(); ++j) {
			if(e.getX()/32 == i & e.getY()/32 == j)
				return true; //if in range
			
			
		}
	}
	return false;
}
	  
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public boolean checkcombatrange(GameCharacter c, MouseEvent e) { //checks the combat range of the attacking game character in relation to the target
	
	for (int i = c.getX()-c.getRange(); i <= c.getX()+c.getRange(); ++i) { //Iterates over the combat range centered on the selected game character
		for (int j = c.getY()-c.getRange(); j <= c.getY()+c.getRange(); ++j) {
			if(e.getX()/32 == i & e.getY()/32 == j)
				return true; //if in rage returns true
			
			
		}
	}
	Main.battleSpace.gameOutputs.append("Not in combat range; ");
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void CombatAction(GameCharacter c, MouseEvent e) {
	if (c.getHasMadeAttack() == false && c.getIsAlive() == true) {
		int targetXPos = e.getX()/32;
		int targetYPos = e.getY()/32;
		
		if(targetXPos <= 24 && targetYPos <= 24) {
			boolean inrange = checkcombatrange(c, e); //checks combat range
			
			if(inrange) { //if in range performs the normal attack
				int AIsize = Main.g.AITeam.size();
				for(int i=0; i<AIsize; i++) {
				GameCharacter Target = AITeam.get(i);
				if (Target.getX() == targetXPos && Target.getY() == targetYPos && Target.getIsAlive()) {
			    	int Damage = c.attack(Dice.RollDice());
			    	Target.HPUpdate(Damage);
			    	c.setHasMadeAttack(true);
			    	Main.battleSpace.repaint();
			    	
			    	if(Damage > 0)
			    		Main.battleSpace.gameOutputs.append("Your " +c.getClassName() + "'s Attack hit. ");

			    	if(Damage == 0)
			    		Main.battleSpace.gameOutputs.append("Your " +c.getClassName() + "'s Attack missed. ");
			    	
			       }
			
				}
		
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void CheckAIHP() { //checks the hitpoint values of the AI team to set the alive flag to false if they are <= 0
	for (GameCharacter c : Main.g.AITeam) {
		if (c.getHitPoints() <= 0) {
			c.setIsAlive(false);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void SecondaryCombatAction(GameCharacter c, MouseEvent e) { // secondary abilities only used by player
	if (c.getHasMadeAttack() == false && c.getIsAlive() == true && c.getHasUsedSecondary()==false) { //ensures the selected character is alive and has not already attacked
		int targetXPos = e.getX()/32;
		int targetYPos = e.getY()/32;
		String CN = c.getClassName(); //Retrieves the class name of the attacking character to ensure the proper ability is used
		
		if(targetXPos <= 24 && targetYPos <= 24) { //ensures the targets are in range
			boolean inrange = checkcombatrange(c, e);
			
			if (inrange) {
		      if (CN.equals("Cleric")) { //Resurrects the selected player character
		    	  int PTsize = Main.g.playerTeam.size();
					for(int i=0; i<PTsize; i++) { //checks over the player team for the target
						GameCharacter Target = playerTeam.get(i);
						if(Target.getX() == targetXPos && Target.getY() == targetYPos) { //checks the selected game character is the actual target
							Target.setIsAlive(true);
							Target.setHP(65); //Returns them with 65 HP and allows them to act
							c.setHasMadeAttack(true);
							c.setCoolDown(true, 5);
							Main.battleSpace.gameOutputs.append("Your " +c.getClassName() + " Raised your " + Target.getClassName() + ". ");
						}
					}
		    	  
		      }
		      
		      if (CN.equals("Fighter")) { //strikes all enemies in 1 square of the fighter
		    	  int AIsize = Main.g.AITeam.size();
					for(int i=0; i<AIsize; i++) {
					GameCharacter Target = AITeam.get(i);
					if (Target.getX() == targetXPos && Target.getY() == targetYPos && Target.getIsAlive()) { //checks the selected game character is the actual target
						int Damage = c.attack(Dice.RollDice());
				    	Target.HPUpdate(Damage);
				    	c.setHasMadeAttack(true);
						c.setCoolDown(true, 3);
						Main.battleSpace.gameOutputs.append("Your " + c.getClassName() + " used Whirlwind; ");
				    	for (GameCharacter x : Main.g.AITeam) {
				    		for (int v = c.getX() - 1; v <= c.getX() + 1; ++v ) {
				    			for (int b = c.getY() - 2; b < c.getY() + 1; ++b ) {
				    				Main.battleSpace.repaint();
				    				if (x.getX() == v && x.getX() == b) {
				    					x.HPUpdate(Damage/2);
				    				}
				    			}
				    		}
				    	}
					}
					}
		    	  
		      }
		      
		      if (CN.equals("Rogue")) { //stuns the enemy character
		    	  int AIsize = Main.g.AITeam.size();
					for(int i=0; i<AIsize; i++) {
					GameCharacter Target = AITeam.get(i);
					if (Target.getX() == targetXPos && Target.getY() == targetYPos && Target.getIsAlive()) { //checks to make sure the target is alive
				    	Target.setIsStuned(true, 2); //stuns target for two turns
				    	c.setHasMadeAttack(true);
						c.setCoolDown(true, 3);
						Main.battleSpace.gameOutputs.append("Your " + c.getClassName() + " Sapped enemy " + Target.getClassName() + "; ");
		    	  
						}
					}
					
		      }
		      if (CN.equals("Mage")) {//does AoE damage on enemies around the target.
			    	  int AIsize = Main.g.AITeam.size();
						for(int i=0; i<AIsize; i++) {
						GameCharacter Target = AITeam.get(i);
						if (Target.getX() == targetXPos && Target.getY() == targetYPos && Target.getIsAlive()) { //checks the selected game character is the actual target
							int Damage = c.attack(Dice.RollDice());
					    	Target.HPUpdate(Damage); //applies full damage to the original target
					    	c.setHasMadeAttack(true);
							c.setCoolDown(true, 4);
							Main.battleSpace.gameOutputs.append("Your " + c.getClassName() + " used Fireball; ");
					    	for (GameCharacter x : Main.g.AITeam) {
					    		for (int v = Target.getX() - 1; v <= Target.getX() + 1; ++v ) {
					    			for (int b = Target.getY() - 2; b < Target.getY() + 1; ++b ) { //Iterates over all squares adjacent to target 
					    				Main.g.grid.Layer5.a[v][b] = 2; //adds fire detail
					    				Main.battleSpace.repaint(); //updates graphics
					    				if (x.getX() == v && x.getX() == b) {
					    					x.HPUpdate(Damage/2); //applies half damage to adjacent enemies
					    				}
					    			}
					    		}
					    	}
						}
					}
			    	  
		      }
		      
		      
			}
			
			
		}
	}
}











///////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                          AI LOGIC                                                     //         
///////////////////////////////////////////////////////////////////////////////////////////////////////////

public void AITurn(GameCharacter ai){

	AIMove(ai);
	AICombat(ai);

	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

public void AIMove (GameCharacter ai) {

	if (CheckIfInCombat(ai) == false) {
		//add check for any enemies in move range.
		if(checkIfEnemyInMoveRange(ai)) {
			moveIntoCombat(ai);
		}
	}
	
	else {
		moveTowardsEnemy(ai);
	}
	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

public void AICombat (GameCharacter ai) {
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

public boolean CheckIfInCombat(GameCharacter ai) {
	int range = ai.getRange();
	int xPos = ai.getX();
	int yPos = ai.getY();
	
	for (int i = xPos - range; i <= xPos + range; i++) {
		for (int j = yPos - range; j <= yPos + range; j++) {
			for (GameCharacter c : playerTeam) {
				if (c.getX() == i && c.getY() == j) {
					System.out.println("In comabt range");
					return true;
				}
			}
		}
	}
	
	System.out.println("not in combat range");
	return false;
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

public boolean checkIfEnemyInMoveRange(GameCharacter ai) {
	int xPos = ai.getX();
	int yPos = ai.getY();
	int moveDistance = ai.getMoveDistance();
	
	for (int i = xPos - moveDistance; i <= xPos + moveDistance; i++) {
		for (int j = yPos - moveDistance; j <= yPos + moveDistance; j++) {
			for (GameCharacter c : playerTeam) {
				if (c.getX() == i && c.getY() == j) {
					System.out.println("In Move Range");
					return true;
				}
			}
		}
	}
	
	System.out.println("Not in move range");
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

public void CheckPlayerHP() {
	for (GameCharacter c : Main.g.playerTeam) {
		if (c.getHitPoints()<= 0) { //check if the player target has been killed
			c.setIsAlive(false); //if has been killed set to dead
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

public void moveIntoCombat(GameCharacter ai) {
	int xPosCur = ai.getX();
	int yPosCur = ai.getY();
	int moveDistance = ai.getMoveDistance();
	int newX;
	int newY;
	
	for (int i = xPosCur - moveDistance; i <= xPosCur + moveDistance; i++) {
		for (int j = yPosCur - moveDistance; j <= yPosCur + moveDistance; j++) {
			for (GameCharacter c : playerTeam) {
				if (c.getX() == i && c.getY() == j) {
					newX = c.getX();
					newY = c.getY() - 1;
					if(!checkCollision(newX, newY)) {
						ai.setX(newX);
						ai.setY(newY);
						updateCollisionLayer(newX, newY, xPosCur, yPosCur);
						
					}
				}
			}
		}
	}
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void moveTowardsEnemy(GameCharacter ai) {
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

public boolean checkCollision(int newX, int newY) {
	if (Main.g.grid.Layer4.a[newX][newY] == 0) {
		return false;
	}
		
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void updateCollisionLayer(int newX, int newY, int xPosCur, int yPosCur) {
	Main.g.grid.Layer4.a[newX][newY] = 1; //sets the new square as occupied to prevent other game characters from stacking in same square
	Main.g.grid.Layer4.a[xPosCur][yPosCur] = 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////

}