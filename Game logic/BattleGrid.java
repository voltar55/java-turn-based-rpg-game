import java.util.*;
import java.awt.image.BufferedImage;



public class BattleGrid {

public Layer Layer1; //graphics layer 1
public Layer Layer2; //graphics layer 2
public Layer Layer3; //graphics layer 3
public Layer Layer4; //collision layer 
public Layer Layer5; //graphics layer 5
public ArrayList<BufferedImage> ImageRepository; //holds all non-sprite graphics
public ArrayList<BufferedImage> SpriteRepository; //holds all sprites used

	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public BattleGrid(String s) {
      Layer1 = new Layer(s, 1);	
      Layer2 = new Layer(s, 2);
      Layer3 = new Layer(s, 3);
      Layer4 = new Layer(s, 4);
      Layer5 = new Layer(s, 5);
      ImageRepository = FillBackground.FillBackgroundPNG();
      SpriteRepository = FillSprites.FillSpriteRepository();
    }	
}
