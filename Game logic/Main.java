import java.awt.*;
import java.awt.event.*;
import javax.swing.*;



public class Main {
	
	private static JFrame mainFrame; //The frame
    private static JButton playButton; //The button that when pressed will make the call to Game
    private static JPanel mainPanel; //The Panel that will hold the button and the dropdown menu 
    public static Game g; //Game object that is referenceable 
    public static String selectedGame = ""; //The arg used by Game to call .txt files
	public static VisualGraphics battleSpace;
	
	
	
	 public static void main(String[] args) {  //creates the JFrame and calls the JPanel method/constructor  

		    mainFrame = new JFrame( "Game Selection" );
	        mainFrame.add(makePanel());
	        mainFrame.setLocationRelativeTo( null );
	        mainFrame.setSize( 200, 100 );
	        mainFrame.setResizable( false );
	        mainFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	        mainFrame.setVisible(true);	
	 }
	 
	 
	 static JPanel makePanel() { //Creates JPanel 
		 
		 String[] GameStrings = {"Demo" , "Forest_Fight" , "City Fight" };
		 JComboBox<String> GameList = new JComboBox<String>(GameStrings);	//creates the drop-down for the game mode choices 	 
		 mainPanel = new JPanel();
		 mainPanel.add(GameList, BorderLayout.PAGE_START); //universally sets the layout 
		 playButton = new JButton ("Play Game");
		 playButton.addActionListener(new ActionListener() {
			 public void  actionPerformed(ActionEvent bp) { //Button press calls the Game constructor
				selectedGame = GameList.getSelectedItem().toString();
				if (selectedGame.contentEquals("Demo")) {
					System.out.println("Demo Selected");
					g = new Game(selectedGame);
					battleSpace = new VisualGraphics(); //Creates graphics object for the demo mode
					System.out.println("Game Object created");
					
				}
				
				if (selectedGame.contentEquals("Forest_Fight")) { //Button calls the Game constructor
					System.out.println("Forest_Fight Selected");
					g = new Game(selectedGame);
					battleSpace = new VisualGraphics(); //Creates graphics object for Forest Fight mode
					System.out.println("Game Object created");
					
				}
				
				if (selectedGame.equals("City Fight")) { //exception for game mode not added
			 		System.out.println("We have yet to add " + selectedGame);
				}

			 }
		 });
		 
		 mainPanel.add(playButton);
		 
		 return mainPanel;
		
	 }
	 
}
	 

