import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class FillBackground {

 public static ArrayList<BufferedImage>	FillBackgroundPNG(){ // Fills the background and other graphics array
	 ArrayList<BufferedImage> a = new ArrayList<BufferedImage>();
	 
	 BufferedImage cliffbottom = null;
	 BufferedImage stonearena = null;
	 BufferedImage explosion = null;
	 BufferedImage dirt = null;
	 BufferedImage treeApprox = null;
	 
	 
	 try {
         cliffbottom = ImageIO.read(new File("BT/tile_cliff_bottom.png"));
         stonearena = ImageIO.read(new File("BT/tile_stone_arena.png"));
         explosion = ImageIO.read(new File("BT/explosion_b_004.png"));
         dirt = ImageIO.read(new File("BT/tiles_013.png"));
         treeApprox = ImageIO.read(new File("BT/tiles_02_007.png"));
        
         
     } catch (IOException e) {
         e.printStackTrace();
     }
	 
	 a.add(cliffbottom); //added in order that the layer draw method will reference and in a manner consistent with the .txts
	 a.add(stonearena);
	 a.add(explosion);
	 a.add(dirt);
	 a.add(treeApprox);
	 
	 return a;
 }
	
}
