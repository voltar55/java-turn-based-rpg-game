import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class FillSprites {

	public static ArrayList<BufferedImage> FillSpriteRepository(){ //fills sprite array
		ArrayList<BufferedImage> a = new ArrayList<BufferedImage>();
		
		BufferedImage knight = null; //names correspond to the image used 
		BufferedImage rogue = null;
		BufferedImage cleric = null;
		BufferedImage mage = null;
		BufferedImage AIknight = null;
		BufferedImage AIrogue = null;
		BufferedImage AIcleric = null;
		BufferedImage AImage = null;
		BufferedImage deadsprite = null; //dead sprite is shard by both teams
		BufferedImage stunedknight = null;
		BufferedImage stunedrogue = null;
		BufferedImage stunedcleric = null;
		BufferedImage stunedmage = null;
		BufferedImage stunedAIknight = null;
		BufferedImage stunedAIrogue = null;
		BufferedImage stunedAIcleric = null;
		BufferedImage stunedAImage = null;
		
		 try {
	            knight = ImageIO.read(new File("GCS/knight.png"));
	            rogue = ImageIO.read(new File("GCS/elf_male.png"));
	            cleric = ImageIO.read(new File("GCS/soldier.png"));
	            mage = ImageIO.read(new File("GCS/wizard_1.png"));
	            AIknight = ImageIO.read(new File("GCS/barbarian.png"));
	            AIrogue = ImageIO.read(new File("GCS/goblin.png"));
	            AIcleric = ImageIO.read(new File("GCS/dwarf_2.png"));
	            AImage = ImageIO.read(new File("GCS/wizard_2.png"));
	            deadsprite = ImageIO.read(new File("GCS/skeleton_die_003.png"));
	            stunedknight = ImageIO.read(new File("GCS/knight_die_003.png"));
	            stunedrogue = ImageIO.read(new File("GCS/dwarf_2_die_003.png"));
	            stunedcleric = ImageIO.read(new File("GCS/soldier_die_003.png"));
	            stunedmage = ImageIO.read(new File("GCS/wizard_1_die_003.png"));
	            stunedAIknight = ImageIO.read(new File("GCS/barbarian_die_003.png"));
	            stunedAIrogue = ImageIO.read(new File("GCS/goblin_die_003.png"));
	            stunedAIcleric = ImageIO.read(new File("GCS/dwarf_2_die_003.png"));
	            stunedAImage = ImageIO.read(new File("GCS/wizard_2_die_003.png"));
	            
	        } catch (IOException e) {
	            System.out.println("error opeening file");
	        }
		 
		 
		 a.add(knight); //adds the buffered images to the array in the same order as the Game Character sprite values 
		 a.add(mage);
		 a.add(cleric);
		 a.add(rogue);
		 a.add(AIknight);
		 a.add(AImage);
		 a.add(AIcleric);
		 a.add(AIrogue);
		 
		 a.add(deadsprite);
         
		 a.add(stunedknight); 
		 a.add(stunedmage);
		 a.add(stunedcleric); 
		 a.add(stunedrogue); 
         a.add(stunedAIknight);
         a.add(stunedAImage);
         a.add(stunedAIcleric); 
         a.add(stunedAIrogue); 
		 
		 return a;
		
	}
	
	
	
	
}
