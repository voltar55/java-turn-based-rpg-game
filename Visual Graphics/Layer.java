import java.awt.Graphics;
import java.util.Scanner;

public class Layer {

	public int[][] a;
	String LayerName;
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public Layer(String s, int x){ //add file reader that uses s and the int to chose the correct file.
		a = new int[24][24];
		String y = s + "/" +s + x +".txt";
		LayerName = "Layer" + x;
		
		try {
			Scanner MapReader = new Scanner (new java.io.FileReader(y));
		
		
		for (int i = 0; i<24; ++i) {
			for (int j = 0; j<24; ++j) {
				a[i][j] = MapReader.nextInt();
			}
			
	    }
		MapReader.close();
		
		}
		catch (java.io.FileNotFoundException e) {
			System.out.println ("Error opening file " + y);
			System.exit(1);
		}
		
	
}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void draw(Graphics g){ //draw method for the individual layer objects
		  for (int i = 0; i <24; ++i)
	        	for (int j = 0; j<24; ++j) {
	        		int k = this.a[i][j];
	        		
	        		if (this.LayerName.equals("Layer5")) {
	        			if (k==2)
	        				g.drawImage(Main.g.grid.ImageRepository.get(k), (i*32), (j*32), null);
	        			else {}
	        		}
	        		else {	
	         	    g.drawImage(Main.g.grid.ImageRepository.get(k), (i*32), (j*32), null);
	        		}
	        		}
	}

	
	
	
}
