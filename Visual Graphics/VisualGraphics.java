import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.Point;




public class VisualGraphics extends JPanel {
	
    private int height;
    private int width;
    private int TileSize;
    public  Point P;
    private static JButton Move;
    private static JButton mainCombat;
    private static JButton endTurn;
    private static JButton secondaryCombat;
    private static JButton clearTextArea;
    public static String movingCharcter = "";
    public static String attackingCharcter = "";
    private int turnCount;
    private static JLabel turnCounter; 
    private static JLabel characterClass;
    private static JLabel characterHP;
    private static JLabel characterMoveInfo;
    private static JLabel characterAttackInfo;
    private static JLabel coolDownStatus;
    public  JTextArea gameOutputs;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public VisualGraphics() { //creates the graphics
       
    	turnCount = 1;
        height = 1000;
        width = 1250;
        TileSize = 32;
        this.setPreferredSize(new Dimension(width, height));
        JFrame frame = new JFrame("BattleSpace");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(this);
        frame.setLayout(null);
        
        String[] PlayerTeam = new String[Main.g.playerTeam.size()]; 
		 int i =0;
		 for(GameCharacter c : Main.g.playerTeam) { 
			 String s = c.getClassName();
			 PlayerTeam[i] = s;
			 ++i;
		 }
		 JComboBox<String> PlayerTeamList = new JComboBox<String>(PlayerTeam); //combobox allows for the user to select GameCharacters to act with
        
        PlayerTeamList.setBounds(800,320,150, 50);
        add(PlayerTeamList);
        PlayerTeamList.addActionListener(new ActionListener () {
        	public void actionPerformed(ActionEvent e) {
        		String SelectedCharacter = PlayerTeamList.getSelectedItem().toString();
        		for (GameCharacter c : Main.g.playerTeam)
        			if (c.getClassName().equals(SelectedCharacter)) {
        				characterClass.setText("Class : " + c.getClassName());
        				characterHP.setText("HP : " + c.getHitPoints());
                		characterMoveInfo.setText("Move Distance : " + c.getMoveDistance()+ " squares");
                		characterAttackInfo.setText("Attack Distance : " + c.getRange() + " squares");
                		coolDownStatus.setText("Cool Down for Secondary ability is : " + c.getCoolDownLength());
        			}
        		
        	}
        });
        
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
        
        Move = new JButton ("Move Action");
        Move.setBounds(800, 100, 150, 50);
        add(Move);
		Move.addActionListener(new ActionListener() { //provides the user anability to select move actions
			 public void actionPerformed(ActionEvent bp) {
				 movingCharcter = PlayerTeamList.getSelectedItem().toString();
	        	 gameOutputs.setText(null); //clears the JText area
	        	 Main.battleSpace.gameOutputs.append("Move Pressed for " + movingCharcter + "; ");
				 dropml();

				 for (GameCharacter c : Main.g.playerTeam) {	
					 if (c.getIsAlive() == false && c.getClassName().equals(movingCharcter))
							Main.battleSpace.gameOutputs.append(c.getClassName() + " is dead; ");
					 
					 if (c.getHasMoved() == true && c.getIsAlive() == true &&c.getClassName().equals(movingCharcter))
							Main.battleSpace.gameOutputs.append(c.getClassName() + " has already moved; ");
					 
					 if (c.getHasMoved() == false && c.getIsAlive() == true &&c.getClassName().equals(movingCharcter)) {
						 addMouseListener(new MouseAdapter() {
					        	public void mousePressed(MouseEvent e) {
					        		Main.g.Movement(c, e);
					       
					        	}
								 
					        });
					   }
					 }
				 }
			 
		 });
		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		 mainCombat = new JButton ("Main Combat Action");
		 mainCombat.setBounds(800, 155, 150, 50);
		 add(mainCombat);
		 mainCombat.addActionListener(new ActionListener() { //allows the user to select the combat action
			 public void  actionPerformed(ActionEvent bp) {
				attackingCharcter = PlayerTeamList.getSelectedItem().toString();
	        	gameOutputs.setText(null); //clears the JText area
				Main.battleSpace.gameOutputs.append("Attack Pressed for " +  attackingCharcter + "; " );
				 dropml();
				 for (GameCharacter c : Main.g.playerTeam) {	 
					 if (c.getIsAlive() == false && c.getClassName().equals(attackingCharcter))
							Main.battleSpace.gameOutputs.append(c.getClassName() + " is dead; ");
					 
					 if (c.getHasMadeAttack() == true && c.getIsAlive() == true && c.getClassName().equals(attackingCharcter))
							Main.battleSpace.gameOutputs.append(c.getClassName() + " has already moved; ");
					 
					 
					 if (c.getHasMadeAttack() == false && c.getIsAlive() == true && c.getClassName().equals(attackingCharcter)) {
						 addMouseListener(new MouseAdapter() {
					        	public void mousePressed(MouseEvent e) {
					        		Main.g.CombatAction(c, e);
					        	    Main.g.CheckAIHP();
					        	    Main.battleSpace.repaint();
					       
					        	}
								 
					        });
						 }

					 }
				 }
		 });
		
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		
		 endTurn = new JButton ("End Player Turn");
		 endTurn.setBounds(800, 265, 150, 50);
		 add(endTurn);
		 endTurn.addActionListener(new ActionListener() { //ends player turn
			 public void  actionPerformed(ActionEvent bp) {
				 Main.battleSpace.gameOutputs.append("End Turn Pressed ");
				 dropml();
	        	 ClearL5();
	        	 gameOutputs.setText(null);
				 turnCounter.setText("Turn: " + ++turnCount);
				 for (GameCharacter c : Main.g.playerTeam) {
					 c.setHasMoved(false);
					 c.setHasMadeAttack(false);
					 if (c.getCoolDownLength() == 0) {
						 c.resetCoolDown(false);
						 
					 }
					 if (c.getHasUsedSecondary()== true && c.getCoolDownLength()>0) { //checks if there is still a cooldown
						 c.UpdateCoolDown(1);
					 }
				 }
				for(GameCharacter c : Main.g.AITeam){  //takes AI turn
					if(c.getHasMadeAttack()==false && c.getHasMoved() == false){
						Main.g.AITurn(c);
						Main.g.CheckPlayerHP();
						Main.battleSpace.repaint();
					}
				}
				for(GameCharacter c : Main.g.AITeam){ //adjusts stun values
					if (c.getStunlength() == 0) {
						c.resetStun(false);
					}
					if(c.getIsStuned()==false) {
						c.setHasMadeAttack(false);
						c.setHasMoved(false);
					}
					
					else {
						c.updateStunlength(1);

					}
				}
				CheckWin(); //checks win condition
				
			 }
		 });
		 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		 
		 secondaryCombat = new JButton ("Secondary ability");
		 secondaryCombat.setBounds(800, 210, 150, 50);
		 add(secondaryCombat);
		 secondaryCombat.addActionListener(new ActionListener() { //Calls secondary combat abilities
			 public void  actionPerformed(ActionEvent bp) {
				 gameOutputs.setText(null);
				 attackingCharcter = PlayerTeamList.getSelectedItem().toString();
			     Main.battleSpace.gameOutputs.append("Secondary Attack Pressed for " +  attackingCharcter + " ");
				 dropml();
				 for (GameCharacter c : Main.g.playerTeam) {	 
					 if (c.getHasMadeAttack() == false && c.getClassName().equals(attackingCharcter)) {
						 addMouseListener(new MouseAdapter() {
					        	public void mousePressed(MouseEvent e) {
					        		Main.g.SecondaryCombatAction(c, e);
					        	    Main.g.CheckAIHP();
					        	    Main.battleSpace.repaint();
					       
					        	}
								 
					        });
						 }
					 }
				 }
		 });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		 clearTextArea = new JButton ("Clear Text Area");
		 clearTextArea.setBounds(1000, 320, 150, 50);
		 add(clearTextArea);
		 clearTextArea.addActionListener(new ActionListener() { //Calls secondary combat abilities
			 public void  actionPerformed(ActionEvent bp) {
				 gameOutputs.setText(null); 
			 }
		   });
		 
		 
		 
		 
		 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		 

		 	gameOutputs = new JTextArea(20,1);
		 	gameOutputs.setBounds(800, 380, 400, 200);
		 	gameOutputs.setVisible(true);
		 	gameOutputs.setEditable(false);
		 	gameOutputs.setLineWrap(true);
		 	gameOutputs.setWrapStyleWord(true);
		 	add(gameOutputs);

	       
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		 
		 
		 
		 

		 turnCounter = new JLabel("Turn: " + turnCount); //adds a visible turn-counter
		 turnCounter.setBounds(800, 10, 100, 100);
		 add(turnCounter);
		 
		 characterClass = new JLabel(); //adds the selected class name
		 characterClass.setBounds(800, 550, 400, 200);
		 add(characterClass);
		 
		 characterHP = new JLabel(); //adds the selected character's HP 
		 characterHP.setBounds(800, 575, 400, 200);
		 add(characterHP);
		 
		 characterMoveInfo = new JLabel(); //adds the selected character's move distance
		 characterMoveInfo.setBounds(800, 600, 400, 200);
		 add(characterMoveInfo);
		 
		 characterAttackInfo = new JLabel(); //adds the selected character's attack distance
		 characterAttackInfo.setBounds(800, 625, 400, 200);
		 add(characterAttackInfo);
		 		 
		 coolDownStatus = new JLabel(); //adds the selected character's cooldown information 
		 coolDownStatus.setBounds(800, 650, 400, 200);
		 add(coolDownStatus);
		 
		 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		 
		 
		 
		 
		 
        frame.pack();
        frame.setVisible(true);
 
        

    }
    
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    public void Display(Graphics g) {
    	
        
        //Draw Layer 1
    	
    	Game game = Main.g;
    	
        game.grid.Layer1.draw(g);
        
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
        
        //Draw Layer 2
        game.grid.Layer2.draw(g);
        	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        		
        //Draw Layer 3
        
        game.grid.Layer3.draw(g);
        
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
        
       for (GameCharacter c : game.AITeam) { //places the AI team on the screen
    	   
    	   if (c.getIsAlive()) {
    		   int i = c.getX();
    		   int j = c.getY();
    		   int s = c.getSpriteVal();
    		   
    		   if(c.getIsStuned() == true) {//places the stuned sprite
    			   s += 9;
            	   g.drawImage(game.grid.SpriteRepository.get(s), (i*TileSize) - 20, (j*TileSize) - 20, null);
    		   }
    		   else { //places the normal sprite
        	   g.drawImage(game.grid.SpriteRepository.get(s), (i*TileSize) - 20, (j*TileSize) - 20, null);
    		   }
    	   	}
    	   else { //places the dead sprite
    		   int i = c.getX();
        	   int j = c.getY();
        	   
        	   g.drawImage(game.grid.SpriteRepository.get(8), (i*TileSize) - 20, (j*TileSize) - 20, null);
        	   
    	   }
        }
       
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
       
       for (GameCharacter c : game.playerTeam) { //places the player team on the screen
    	   if (c.getIsAlive()) {
    	   int i = c.getX();
    	   int j = c.getY();
    	   int s = c.getSpriteVal();
    	   
   		   if(c.getIsStuned() == true) { //uses the stuned sprite
			   s += 9;
        	   g.drawImage(game.grid.SpriteRepository.get(s), (i*TileSize) - 20, (j*TileSize) - 20, null);
		   }
		   else { //uses the normal sprite
    	   g.drawImage(game.grid.SpriteRepository.get(s), (i*TileSize) - 20, (j*TileSize) - 20, null);
		   }
    	}
    	   else { //places the dead sprite
    		   int i = c.getX();
        	   int j = c.getY();
        	   
        	   g.drawImage(game.grid.SpriteRepository.get(8), (i*TileSize) - 20, (j*TileSize) - 20, null);
        	   
    	   }
       }
       
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
       
       //Draw Layer 5 effects
       game.grid.Layer5.draw(g);
       
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
        
        //Draw Grid Lines
        g.setColor(Color.GRAY); //puts down grid lines 
        for (int i = 0; i <24; ++i) {
        	for (int j = 0; j <24; ++j) {
        		g.drawRect(i*TileSize, j*TileSize, TileSize, TileSize);
        	}
        }
        
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
        
		   g.setColor(Color.BLUE); //Boxes the friendlies in blue
		   for (GameCharacter c : game.playerTeam) {
	    	   int i = c.getX();
	    	   int j = c.getY();
	    	   
	    	   g.drawRect(i*TileSize, j*TileSize, TileSize, TileSize);
		   }

		   
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		  
		   
		   
		   g.setColor(Color.RED); //Boxes the enemy in red
		   for (GameCharacter c : game.AITeam) {
	    		   int i = c.getX();
	    		   int j = c.getY();
	    		   
	    		   g.drawRect(i*TileSize, j*TileSize, TileSize, TileSize);
	    	   }
		   }
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
   
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Display(g);
    }
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    
    public void dropml() { //clears the mouse listeners from previous button presses/turns
		 MouseListener[] mouseListeners = Main.battleSpace.getMouseListeners();
		 for (MouseListener mouseListener : mouseListeners) {
			 Main.battleSpace.removeMouseListener(mouseListener);
		 }
		
    }
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private void ClearL5() { //clears the graphics off layer 5
    	int l = Main.g.grid.Layer5.a.length;
    	
    	for (int i = 0; i < l; ++i)
    		for (int j =0; j < l; ++j)
    			Main.g.grid.Layer5.a[i][j] = 0;
    }
    
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private void CheckWin() { //checks if all members of a team is dead
    	int p = 0;
    	int a = 0;
    	for (GameCharacter c : Main.g.playerTeam) {
    		if (c.getIsAlive() == false)
    			++p;
    	}
    	if (p == 4)
    		Main.battleSpace.gameOutputs.append("You Lose Every One Died..... ");
    	
    	
    	for (GameCharacter c : Main.g.AITeam) {
    		if (c.getIsAlive() == false)
    			++a;
    	}
    	if (a == 4)
    		Main.battleSpace.gameOutputs.append("You Win Since You Killed Everyone Else..... ");
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
    
}