
public class Mage extends GameCharacter{
	
    private int healthPoints;
//    private int strength;
//    private int dexterity;
    private int will;
    private int x;
    private int y;
//    private boolean team;
    private int spriteNumber;
    public String ClassName;
    private final int range;
    private final int travelD;
    private boolean HasMoved;
    private boolean HasMadeAttack;
    private boolean IsAlive;
    private boolean IsStuned;
    private int Stunlength;
    private int CoolDown;
    private boolean HasUsedSecondary;

    public Mage(){
         healthPoints = 100;
//         strength = 2;
//         dexterity = 6;
         will = 10;
         spriteNumber = 1;
         x = 0;
         y = 0;
         range = 4;
         travelD = 3;         
         ClassName = "Mage";
         HasMoved = false;
         HasMadeAttack = false;
         IsAlive = true;
         IsStuned = false;
         Stunlength = 0;
         CoolDown = 0;
         HasUsedSecondary = false;
         
    }
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                             methods described in abstract                                                    //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public int getRange() {
    	return range;
    }
    public void isAIChar() {
    	spriteNumber = spriteNumber + 4;
    }
    public int attack(int dice){ //uses dice class to determine hit and based on roll applies damage adjust by prime stat
        int damage;
        if(dice < 3){
            damage = 0;
        }
        else if(dice >= 3 && dice <= 17){
            damage = will*2;
        }
        else{
            damage = will*4;
        }
    return damage;
    }
    public void setX(int newPosX){
        this.x = newPosX;
    }
    public void setY(int newPosY){
        this.y = newPosY;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return this.y;
    }
    public void HPUpdate(int attack){
        this.healthPoints = healthPoints - attack;
    }
    public int getSpriteVal(){
        return this.spriteNumber;
    }
    
    public int getHitPoints() {
    	return healthPoints;
    }
    
    public String getClassName() {
    	return ClassName;
    }
    
    public boolean getHasMoved() {
    	return HasMoved;
    }
    
    public void setHasMoved(boolean b) {
    	HasMoved = b;
    }
    public boolean getHasMadeAttack() {
    	return HasMadeAttack;
    }
    
    public void setHasMadeAttack(boolean b) {
    	HasMadeAttack = b;
    }
    
    public int getMoveDistance() {
    	return travelD;
    }
    
    public boolean getIsAlive() {
    	return IsAlive;
    }
    
    public void setIsAlive(boolean b) {
    	IsAlive = b;
    }
    public boolean getIsStuned () {
    	return IsStuned;
    }
    
    public void setIsStuned(boolean b, int x) {
    	IsStuned = b;
    	Stunlength = x;
    }
    
    public void setHP(int x) {
    	healthPoints = x;
    }
    public void updateStunlength(int x) {
    	Stunlength -= x;
    }
    public int getStunlength() {
    	return Stunlength;
    }
    public int getCoolDownLength() {
    	return CoolDown;
    	
    }
    public void setCoolDown(boolean b, int x) {
    	CoolDown = x;
    	HasUsedSecondary = b;
    	
    }
    public void UpdateCoolDown(int x) {
    	CoolDown -= x;
    	
    }
    public boolean getHasUsedSecondary() {
    	return HasUsedSecondary;
    }
    
    public void resetCoolDown(boolean b) {
    	HasUsedSecondary = b;
    	
    }
    public void resetStun(boolean b) {
    	IsStuned = b;
    }
    
}


