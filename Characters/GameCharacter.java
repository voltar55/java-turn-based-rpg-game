public abstract class GameCharacter{ //Abstract class that all the RPG class will extend
   
	public abstract void setX(int newPosX); //sets the current X position 
    public abstract void setY(int newPosY); //sets the current Y position
	public abstract int getX(); //Retrieves the X position for other methods to reference
	public abstract int getY(); //Retrieves the y position for other methods to reference
	public abstract void HPUpdate(int Damage); //applies damage to the Game Character
	public abstract int getHitPoints(); // //Retrieves the current HP for other methods to reference
	public abstract int attack(int dice); //basic attack method for all game characters
	public abstract int getSpriteVal(); ////Retrieves the Sprite value for the draw methods to reference
	public abstract void isAIChar(); //applies a flag if not the player character and updates the sprite value
	public abstract int getRange(); //Retrieves the attack range for other methods to reference
	public abstract String getClassName(); //Retrieves the class name for other methods to reference
	public abstract boolean getHasMoved(); //Retrieves if the character has moved for other methods to reference
	public abstract void setHasMoved(boolean b); //sets the moved value after a successful move
	public abstract boolean getHasMadeAttack(); //Retrieves if the game character has attacked for other methods to reference
	public abstract void setHasMadeAttack(boolean b); //sets the attacked value after a successful attack
	public abstract int getMoveDistance(); //Retrieves the move distance of the game character for other methods to reference
	public abstract boolean getIsAlive(); //Retrieves if the game character is alive for other methods to reference
	public abstract void setIsAlive(boolean b); //sets if the game character is alive
	public abstract boolean getIsStuned(); //Retrieves if the game character is stunned for other methods to reference
	public abstract void setIsStuned(boolean b, int x); //sets the stun value for the game character and the length of the stun
	public abstract void updateStunlength(int x); //updates the stun length (normally only reduces)
	public abstract int getStunlength(); //Retrieves how long the character is stunned for other methods to reference
	public abstract void setHP(int x); //sets the HP value for the game character 
	public abstract int getCoolDownLength(); //Retrieves the cool down time for other methods to reference
	public abstract void setCoolDown(boolean b, int x); //sets the cool down length for the game character
	public abstract void resetCoolDown(boolean b); //allows the boolean to be flipped back
	public abstract boolean getHasUsedSecondary(); //checks if the game character has used its secondary ability 
	public abstract void UpdateCoolDown(int x); //updates the cool down length (normally only reduces)
	public abstract void resetStun(boolean b); //allow the stuned flag to be removed;
    
}